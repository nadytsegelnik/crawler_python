import re
import urllib
import urllib.request
import validators

listOfLinks = list()
setOfLinksAndStatus = set()
startURL = "http://www.epam.com"
fileName = 'text.txt'
regex = '.*?href="(.*?)"'


class MiniHTMLParser:
    def getStatusOfHtmlFile(self, url):
        status = ""
        try:
            code = urllib.request.urlopen(url).getcode()
            if code not in [200, 301]:
                status = "invalid"
            else:
                status = "valid"
        except urllib.request.URLError:
            status = "invalid"
            print("invalid zhtml.")
        return status

    def writeFile(self, html):
        file = open(fileName, 'a')
        file.write(str(html))
        file.close()

    def readHTML(self, url):
        try:
            page = urllib.request.urlopen(url)
            html = page.read()
            page.close()
            links = re.findall(regex, str(html))
            for link in links:
                if (startURL + link) not in listOfLinks:
                    if not re.search(u'(.*?http)|(.*?mailto)|(.*?javascript)|(.*?ico)|(.*?etc)', link):
                        listOfLinks.append(startURL + link)
        except urllib.request.URLError:
            print("Error in url")
        return listOfLinks


def main():
    parser = MiniHTMLParser()
    MiniHTMLParser.readHTML(parser, startURL)
    for link in listOfLinks:
        MiniHTMLParser.readHTML(parser, link)
        MiniHTMLParser.writeFile(parser, MiniHTMLParser.getStatusOfHtmlFile(parser, link) + " " + link)


if __name__ == "__main__":
    main()
